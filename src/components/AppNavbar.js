import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Link, NavLink } from 'react-router-dom';
import { useState, Fragment } from 'react';
import Logout from '../pages/Logout';

export default function AppNavbar(){
	const [user, setUser] = useState(localStorage.getItem("email")); // s53
	console.log(user);
	console.log(setUser);

    return (
        <Navbar bg="light" expand="lg">
		    <Container fluid>
		        <Navbar.Brand as={Link} to="/" >Zuitt Booking (from AppNavbar.js)</Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav"/>
		        <Navbar.Collapse id="basic-navbar-nav">
		            <Nav className="mr-auto">
						<Nav.Link as={NavLink} to="/">Home</Nav.Link>
						<Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>
						{(user !== null) ?
							<Nav.Link as={NavLink} to="/logout">Logout {user}</Nav.Link>
							:
							<Fragment>
								<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
								<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
							</Fragment>
						}
		            </Nav>
		        </Navbar.Collapse>
		    </Container>
		</Navbar>
    )
}
