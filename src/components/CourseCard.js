// import Button from 'react-bootstrap/Button';
// import Card from 'react-bootstrap/Card';
// Other import format
import{ Button, Card } from 'react-bootstrap';
import{ useState, useEffect } from 'react';
import PropTypes from 'prop-types';

export default function CourseCard({courseProp}) {
  // console.log("Contents of props: ");
  // console.log(props);
  // console.log(typeof props);

  const {name, description, price} = courseProp;

  // State Hooks (useState) - a way to store information within a component and tract this information
      // getter, setter
      // variable, function to change the value of a variable
  // const [count, setCount] = useState(0); // count = 0

  // function enroll(){
  //   setCount(count + 1);
  //   console.log('Enrollees: ' + count);
  // }

  const [count, setCount] = useState(0);
  const [seats, setSeat] = useState(30);
  function enroll(){
    // if(seats > 0){
    //   setCount(count + 1);
    //   setSeat(seats - 1);
    //   console.log('Enrollees: ' + count);
    //   console.log('Seats: ' + seats);
    // }
    // else{
    //   alert("No more seats");
    // }
    setCount(count + 1);
    console.log('Enrollees: ' + count);
    setSeat(seats - 1);
    console.log('Seats: ' + seats);  
  }

  // useEffect is another Hook State
  // useEffect() always runs the task on the initial render and/or every render when the state changes in a component
  // Initial render is when the component is run or displayed for the first time
  useEffect(() => {
    if(seats === 0){
      alert('No more seats available');
    }
  }, [seats])


  return (
    <Card className='mt-2'>
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>{price}</Card.Text>
        <Button variant="primary" onClick={enroll}>Enroll</Button>
        <Card.Text>Total Enrolled: {count}</Card.Text>
      </Card.Body>
    </Card>
  )
}

// Check if the CourseCard component is getting the correct property
CourseCard.propTypes = {
  courseProp: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired
  })
}