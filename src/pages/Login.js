import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';


export default function Login(){

    // 3 use the state
    // variable, setter function
    const { user, setUser } = useContext(UserContext);
    console.log(UserContext);

    // State hooks to store the values of the input fields
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [isActive, setIsActive] = useState(false);

    console.log(email);
    console.log(password1);

    function loginUser(event){
        event.preventDefault();
        localStorage.setItem('email', email); // s53

        setUser({
            email: localStorage.getItem('email')
        })

        setEmail('');
        setPassword1('');
        alert('Login Successful');
        console.log(`${email} has been successfully login`);
    }
    useEffect(() => {
        if(email !== '' && password1 !== ''){
            setIsActive(true);
        }
        else{
            setIsActive(false);
        }
    }, [email, password1])

    return(
        
    (user.email !== null)? // true - it means email field is successfully set
        <Navigate to ="/courses" />
    :
        <Form onSubmit={(event) => loginUser(event)}>
        <h2 className='pt-3'>Login</h2>
            <Form.Group controlId="userEmail" className='pt-2'>
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email" 
                    value = {email}
                    onChange = {e => setEmail(e.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password1" className='pt-2'>
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    value = {password1}
                    onChange = {e => setPassword1(e.target.value)}
                    required
                />
            </Form.Group>

            <div className='pt-3'>
                { isActive ?
                    <Button variant="success" type="submit" id="submitBtn">
                        Login
                    </Button>
                    :
                    <Button variant="success" type="submit" id="submitBtn" disabled>
                        Login
                    </Button>
                }  
            </div>
        </Form>    
    )
}
