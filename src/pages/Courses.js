import coursesData from "../data/coursesData";
import CourseCard from '../components/CourseCard';
import { Fragment } from 'react';


export default function Courses() {
    console.log("Contents of coursesData");
    console.log(coursesData);
    console.log(coursesData[0]);

    // array method to retrieve all mock data courses
    const courses = coursesData.map(course => {
        return (
            <CourseCard key={course.id} courseProp={course} />
        )
    })
    
    // return(
    //     <Fragment>
    //         <CourseCard courseProp={coursesData[0]} />
    //         {/* {coursesData.map(course => <CourseCard course={course} />)} */}
    //     </Fragment>
    // )

    return (
        <Fragment>
            {courses}
        </Fragment>
    )
}